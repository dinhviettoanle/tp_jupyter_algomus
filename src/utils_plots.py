import plotly.graph_objects as go
import pandas as pd

def make_composers_plot():
    # Source : https://www.audiolabs-erlangen.de/resources/MIR/cross-comp
    # --- Import data ---
    df_composers = pd.read_csv('data/cross-composer_annotations.csv', sep=',')
    df_select = df_composers.groupby(['Composer', 'Instrumentation'], as_index=False).count()[['Composer', 'Instrumentation', 'Class']]
    
    list_composers = ['Rameau; Jean-Philippe', 'Bach; Johann Sebastian', 'Handel; George Frideric', 
                     'Haydn; Franz Joseph', 'Mozart; Wolfgang Amadeus', 'Beethoven; Ludwig van', 
                     'Schubert; Franz', 'Mendelssohn; Felix', 'Brahms; Johannes', 'Dvorak; Antonin', 
                     'Shostakovich; Dmitry']

    list_era = ['baroque', 'baroque', 'baroque', 
                'classical', 'classical', 'classical',
                'romantic', 'romantic', 'romantic', 'romantic',
                'modern']

    list_instrumentation = ['orchestra', 'piano', 'ensemble', 'choral', 'organ', 'solo']

    col_composers = sum([[composer]*len(list_instrumentation) for composer in list_composers], [])
    col_instrumentation = list_instrumentation * len(list_composers)
    col_count = [0]*len(col_composers)
    
    # --- Clean Data ---
    df_clean = pd.DataFrame()
    df_clean['composer'] = col_composers
    df_clean['instrumentation'] = col_instrumentation
    df_clean['count'] = col_count
    
 
    for i in range(len(df_select)):
        this_composer = df_select.iloc[i]['Composer']
        this_count = df_select.iloc[i]['Class']
        list_ensembles = df_select.iloc[i]['Instrumentation'].split(";")
        for ens in list_ensembles:
            if ens in list_instrumentation:
                df_clean.loc[(df_clean.composer == this_composer) & (df_clean.instrumentation == ens), 'count'] += this_count
            else:
                df_clean.loc[(df_clean.composer == this_composer) & (df_clean.instrumentation == 'solo'), 'count'] += this_count
    
    # --- Plot ---
    fig = go.Figure()

    for i, instrument in enumerate(reversed(list_instrumentation)):
        y = df_clean.loc[df_clean.instrumentation == instrument]['count'].tolist()
        custom_data = [[instrument, era] for era in list_era]

        fig.add_trace(go.Bar(
            x=list_composers, y=y, name=instrument,
            customdata=custom_data, 
            hovertemplate=
                "<b>Composer</b> : %{x} <br>" + 
                "<b>Era</b> : %{customdata[1]} <br>" + 
                "<b>Instrumentation</b> : %{customdata[0]} <br>" +
                "<b>Count</b> : %{y}"
        ))

    fig.update_layout(barmode='stack', title='# of pieces by composer')
    fig.show()